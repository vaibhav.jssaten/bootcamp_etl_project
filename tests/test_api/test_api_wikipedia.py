import unittest

from bootcamp_cri.api.api_wikipedia import get_author_info_from_wikipedia
from tests.test_api.fixtures import JSON_RESPONSE


class TestApiWikipedia(unittest.TestCase):

    def test_get_author_infos_from_wikipedia(self):
        # Given
        author_name = "Donato Carrisi"

        # When
        expected_json_response = JSON_RESPONSE

        json_response = get_author_info_from_wikipedia(author_name)

        # Then
        self.assertEqual(expected_json_response, json_response)

